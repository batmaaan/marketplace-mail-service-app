package org.marketplace.mail.client

import com.beust.klaxon.Klaxon
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.future.future
import org.marketplace.mail.model.MessageRequest
import org.marketplace.mail.model.MessageResponse
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper
import java.util.concurrent.CompletableFuture

class MailServiceClientImpl(private val url: String):MailServiceClient {
  private val client = HttpClient(OkHttp)


  @OptIn(DelicateCoroutinesApi::class)
  override fun sendMessage(request: MessageRequest): CompletableFuture<MessageResponse?> {
    val objectMapper = ObjectMapper()
    val requestBody: String = objectMapper
      .writeValueAsString(request)
    val URI = "$url/send_message"

    return GlobalScope.future {
      val response = client.post {
        url(URI)
        contentType(ContentType.Application.Json)
        header(HttpHeaders.ContentType, ContentType.Application.Json)
        setBody(requestBody)
      }
      val responseBody: String = response.body()

      Klaxon().parse<MessageResponse>(responseBody)
    }
  }
}
