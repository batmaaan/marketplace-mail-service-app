package org.marketplace.mail.client

import org.marketplace.mail.model.MessageRequest
import org.marketplace.mail.model.MessageResponse
import java.util.concurrent.CompletableFuture

interface MailServiceClient {
  fun sendMessage(request: MessageRequest): CompletableFuture<MessageResponse?>
}
