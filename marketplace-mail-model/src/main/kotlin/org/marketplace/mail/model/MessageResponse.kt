package org.marketplace.mail.model

import kotlinx.serialization.Serializable

@Serializable
data class MessageResponse
  (
  val status: String,
  val description: String
)
