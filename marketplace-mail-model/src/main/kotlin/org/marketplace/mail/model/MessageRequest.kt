package org.marketplace.mail.model

import kotlinx.serialization.Serializable

@Serializable
data class MessageRequest(
  val email: String,
  val title: String,
  val text: String
)
