package org.marketplace.mail.service.kafka

import com.google.gson.Gson
import com.typesafe.config.ConfigFactory
import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.Email
import org.apache.commons.mail.HtmlEmail
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.requests.DeleteAclsResponse.log
import org.marketplace.mail.model.MessageRequest
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import java.util.*
import java.util.List
import kotlin.collections.set

private val topics = List.of("Message")
private val conf = ConfigFactory.load()
private val templateEngine = TemplateEngine()

fun startConsumer() {
  val context = Context()
  templateEngine.setTemplateResolver(ClassLoaderTemplateResolver().apply {
    prefix = "templates/"
    suffix = ".html"
    characterEncoding = "utf-8"
  })
  val props = Properties()
  props["bootstrap.servers"] = "localhost:9092"
  props["key.deserializer"] = "org.apache.kafka.common.serialization.StringDeserializer"
  props["value.deserializer"] = "org.apache.kafka.common.serialization.StringDeserializer"
  props["group.id"] = "1"
  try {
    KafkaConsumer<String?, String?>(props).use { consumer ->
      consumer.subscribe(topics)
      while (true) {
        val records: ConsumerRecords<String?, String?>? = consumer.poll(100)
        for (record in records!!) {
          val request = Gson().fromJson(record.value(), MessageRequest::class.java)
          context.setVariables(mapOf("request" to request.text))
          val message = templateEngine.process("message", context)
          println(message)
          try {
            val email: Email = HtmlEmail()
            email.hostName = conf.getString("EmailHostname")
            email.setSmtpPort(587)
            email.setAuthenticator(
              DefaultAuthenticator(
                conf.getString("EmailUserName"),
                conf.getString("EmailUserPass")
              )
            )
            email.isSSLOnConnect = true
            email.setFrom(conf.getString("EmailUserName"))
            email.subject = request.title
            email.setMsg(message)
            email.addTo(request.email)
            email.send()
          } catch (e: Exception) {
            e.printStackTrace()
          }
        }
      }
    }
  } catch (e: Exception) {
    log.error("Consumer error", e)
  }
}
