//package org.marketplace.mail.service.controller
//
//import com.typesafe.config.ConfigFactory
//import io.ktor.server.application.*
//import io.ktor.server.request.*
//import io.ktor.server.response.*
//import org.apache.commons.mail.DefaultAuthenticator
//import org.apache.commons.mail.Email
//import org.apache.commons.mail.HtmlEmail
//import org.marketplace.mail.model.MessageRequest
//import org.marketplace.mail.model.MessageResponse
//import org.thymeleaf.TemplateEngine
//import org.thymeleaf.context.Context
//import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
//
//
//class MessageController(private val call: ApplicationCall) {
//  private val templateEngine = TemplateEngine()
//  private val conf = ConfigFactory.load()
//
//
//  suspend fun sendMessage() {
//    val context = Context()
//    val request = call.receive<MessageRequest>()
//    templateEngine.setTemplateResolver(ClassLoaderTemplateResolver().apply {
//      prefix = "templates/"
//      suffix = ".html"
//      characterEncoding = "utf-8"
//    })
//    context.setVariables(mapOf("request" to request.text))
//    val message = templateEngine.process("message", context)
//
//    try {
//      val email: Email = HtmlEmail()
//      email.hostName = conf.getString("EmailHostname")
//      email.setSmtpPort(587)
//      email.setAuthenticator(DefaultAuthenticator(conf.getString("EmailUserName"), conf.getString("EmailUserPass")))
//      email.isSSLOnConnect = true
//      email.setFrom(conf.getString("EmailUserName"))
//      email.subject = request.title
//      email.setMsg(message)
//      email.addTo(request.email)
//      email.send()
//      call.respond(MessageResponse("OK", "Message sent!"))
//    } catch (e: Exception) {
//      e.printStackTrace()
//      call.respond(MessageResponse("ERROR", "Something wrong, try again!"))
//    }
//  }
//}
